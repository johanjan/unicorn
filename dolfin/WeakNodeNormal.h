#ifndef WEAKNODENORMAL_H
#define WEAKNODENORMAL_H

#include <dolfin.h>

#include <ufc2/NormalProjection.h>

namespace dolfin
{

class WeakNodeNormal
{
public:
  WeakNodeNormal(Mesh& mesh, Function* innormal = 0)
    {
        an = new NormalProjectionBilinearForm;
	Ln = new NormalProjectionLinearForm;

	if(innormal == 0)
	  normal = new Function;
        normal->init(mesh, *an, 0);

        pde = new LinearPDE(*an, *Ln, mesh);
        pde->solve(*normal);
    }
    virtual ~WeakNodeNormal()
    {
        delete an;
        delete Ln;
        delete pde;

// warning I'm not sure about this
//        delete normal;
    }

    Function* getNormal() const {return normal;}

private:
    Function* normal;

    NormalProjectionBilinearForm* an;
    NormalProjectionLinearForm* Ln;
    LinearPDE* pde;
};

}

#endif /* end of include guard: WEAKNODENORMAL_H */
