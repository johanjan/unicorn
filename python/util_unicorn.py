#Copyrights
#Niyazi Cem Degirmenci

from dolfin import *
from mshr import *
import numpy as np

def plot_compact(u,p, beta ,theta ,t, stepcounter, Q, V, file_u, file_p, file_beta, file_theta): # Compact plot utility function
    if stepcounter % 20 == 0:
	 u_p = project(u, V);
	 p_p = project(p, Q);
	 u_p.rename("velocity", "label");
	 p_p.rename("pressure", "label");
	 beta.rename("mesh_velocity", "label");
	 theta.rename("phase", "label");
   	 file_u << u_p;
	 file_p << p_p;
	 file_beta << beta;
	 file_theta << theta;


def dofs_to_verts(mesh, V, Vv):
    """ 
    Compute the dof to vert mapping
    original Function by Johan Hake from https://answers.launchpad.net/dolfin/+question/216695
    slightly modified by Cem
    """
    coords = mesh.coordinates()
    dim = mesh.geometry().dim()

    # Dof arrays
    vert_to_dofs = np.zeros(mesh.num_vertices(), dtype=np.uintp)

    dofs_to_vert = np.zeros(mesh.num_vertices(), dtype=np.uintp)
    vectordofs_to_vert = np.zeros((mesh.num_vertices()*dim), dtype=np.uintp)
    vectordofs_to_subvert = np.zeros((mesh.num_vertices()*dim),
dtype=np.uintp)

    # DofMaps
    dm = V.dofmap()
    dms = [Vv.sub(i).dofmap() for i in range(dim)]

    # Iterate over cells and collect dofs
    for cell in cells(mesh):
        cell_ind = cell.index()
        vert_inds = cell.entities(0)

        # Check that dof coordinates are the same as the vertex coordinates
        #assert(np.all(coords[vert_inds]==dm.tabulate_coordinates(cell)))
        dofs_to_vert[dm.cell_dofs(cell_ind)] = vert_inds
	vert_to_dofs[vert_inds] = dm.cell_dofs(cell_ind)

        # Iterate over the sub dof maps
        for i, dms_i in enumerate(dms):
            vectordofs_to_vert[dms_i.cell_dofs(cell_ind)] = vert_inds
            vectordofs_to_subvert[dms_i.cell_dofs(cell_ind)] = i 

    # Return dof to vertex mapping
    return vert_to_dofs, dofs_to_vert, vectordofs_to_vert, vectordofs_to_subvert



def solid_move(m, beta, vectordofs_to_vert, vectordofs_to_subvert, vert_to_dofs, mask):	
	# function expects a mesh, a CG1v function with dim = mesh.geometry().dim() and 
	# the maps that are calculated with dofs_to_verts utility
	# moves the solid part of the  mesh to beta
	betavec = np.array(beta.vector())
	maskvec = np.array(mask.vector())

	for dof in range(betavec.size):
		dofval = betavec[dof]
		vertind = vectordofs_to_vert[dof]
		x_or_y  = vectordofs_to_subvert[dof]

		maskdof = vert_to_dofs[vertind]
		maskval = maskvec[maskdof]
		if maskval == 1.:
			m.coordinates()[vertind][x_or_y] =  dofval

def mesh_move(m, beta, vectordofs_to_vert, vectordofs_to_subvert):	
	# function expects a mesh, a CG1v function with dim = mesh.geometry().dim() and 
	# the maps that are calculated with dofs_to_verts utility
	# moves the whole mesh to beta
	betavec = np.array(beta.vector())
	for dof in range(betavec.size):
		dofval = betavec[dof]
		vertind = vectordofs_to_vert[dof]
		x_or_y  = vectordofs_to_subvert[dof]
		m.coordinates()[vertind][x_or_y] = dofval
	
	
def laplacian_smooth(m, theta, flowvel, V ):
	# function to compute a smoothed mesh motion where 
	# beta = u in solid part (theta = 0) and 
	# (\nabla beta, \nabla v ) = 0  in fluid part
	class All(SubDomain):
		def inside(self, x, on_boundary):
    			return ( on_boundary)	

#	class Solid(SubDomain):
#		def inside(self, x, on_boundary):
#			narr = np.array([0], dtype=float)
#			xarr = np.array([x[0], x[1]], dtype=float)
#			theta.eval(narr, xarr)
#			return (narr[0] == 0.0)

	zerovel = Constant((0.,0.))
	allb = All()
#	solidb = Solid()

	bc = DirichletBC(V, zerovel, allb)

#	bc2 = DirichletBC(V, flowvel, solidb)

#	f = Expression(("0.0", "0.0"))
	h = CellSize(m);
	u = TrialFunction(V)
	v = TestFunction(V)
	a = inner(grad(u), grad(v))*dx + inner(u,v)*(1./h)*(1. - theta) *dx
	L = inner(flowvel, v)*(1./h)*(1.-theta )*dx
	ux = Function(V)
	#solve(a==L, ux , [bc,bc2]) -> mesh entity problem
	solve(a==L, ux , bc)
	return ux



def update_stress(m, flowvel, S0v, mu, DG0m):
	S0 = as_matrix([[S0v[0], S0v[2]],[S0v[1], S0v[3]]])
	u = TrialFunction(DG0m)
	v = TestFunction(DG0m)
	vm = as_matrix([[v[0], v[2]],[v[1], v[3]]])
	
	eps = 0.5* (grad(flowvel) + transpose(grad(flowvel)))
	Dobj = grad(flowvel)*S0 + S0*transpose(grad(flowvel))
	a = inner(u,v)*dx
	L = inner(2*mu*eps + Dobj, vm)*dx
	ux = Function(DG0m)
	solve(a==L, ux)
	return ux



if __name__ == "__main__":
	XMIN = 0.; XMAX = 4.; YMIN = 0; YMAX = 1.; G = [XMIN, XMAX, YMIN, YMAX]; eps = 1e-5 # Geometry and mesh
	
	BARTHICK = 0.05
	BARLENGTH = 1.0 
	CIRCLECENTREX = 0.5 
	CIRCLECENTREY = 0.5 
	CIRCLERADIUS = 0.1 
	BARLEFT = CIRCLECENTREX
	BARRIGHT = CIRCLECENTREX + BARLENGTH
	BARTOP = CIRCLECENTREY + BARTHICK/2.0
	BARBOTTOM = CIRCLECENTREY - BARTHICK/2.0
	BARGEOM = Rectangle(Point(BARLEFT, BARBOTTOM), Point(BARRIGHT, BARTOP))
	CIRCLEGEOM = Circle(Point(CIRCLECENTREX, CIRCLECENTREY),CIRCLERADIUS)
	BOXGEOM = Rectangle(Point(G[0], G[2]), Point(G[1], G[3])) - CIRCLEGEOM
	BOXGEOM.set_subdomain(1,BARGEOM)
	mesh = generate_mesh(BOXGEOM, 30) 

	for i in range(0, 2): # Refine mesh
    		cell_markers = MeshFunction("bool", mesh, mesh.topology().dim());
    		for c in cells(mesh):
        		mp = c.midpoint()
        		cell_markers[c] = abs(mp[0]) <= 1.5 and abs(mp[1] - (YMAX/2.0)) <= (0.3*YMAX) and c.diameter() > 0.05
        		cell_markers[c] = cell_markers[c] or ((mp[0] < BARRIGHT) and (mp[0] > BARLEFT+ 0.15) and (mp[1] < BARTOP) and (mp[1] > BARBOTTOM))
    		mesh = refine(mesh, cell_markers)
	m=mesh
	V = FunctionSpace(m,"CG",1)
	Vv = VectorFunctionSpace(m, "CG", 1)
	vert_to_dofs, dofs_to_vert, vectordofs_to_vert, vectordofs_to_subvert =  dofs_to_verts(m, V, Vv)
	beta = Function(Vv)
	beta.interpolate(Expression(("x[0]","x[1]"), degree=1))
	betavec = np.array(beta.vector())
	
	for dof in range(betavec.size):
		dofval = betavec[dof]
		vertind = vectordofs_to_vert[dof]
		x_or_y  = vectordofs_to_subvert[dof]
		meshval = m.coordinates()[vertind][x_or_y]
		if (dofval <> meshval):
			print "probleeem !!!!!!!!!!!!!!!!!!", dofval, " " , meshval
			exit()
		print " ok dofval : " , dofval, "meshval : ", meshval




