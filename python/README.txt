Unicorn Python prototypes
-------------------------

Instructions
============

We suggest running a Docker container environment to run the Python
prototypes. To start the supported Docker container, type in a shell:

sudo docker run -t -i -p 81:8000 jjan/fenics-mooc:test

If all goes well you can then connect with your webserver by going to the address:

localhost:81

You can then login as user0 with password from the Docker log that you
see in the terminal you started Docker in.

Then create a new Python 2 notebook in Jupyter, and copy and paste
your Python prototype, for example "fsi.py", and run the notebook.

Right now plotting is not implemented in Jupyter, you can visualize
the .vtu output files that appear in the Docker filesystem, which can
be accessed at:

/var/lib/docker/aufs/mnt/<filesystemID>

Contact (for the Python prototypes)
=======

Johan Jansson (jjan@kth.se)
