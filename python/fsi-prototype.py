# Copyright (C) 2016-2017 Johan Jansson and Niyazi Cem Degirmenci
# Licensed under the GNU LGPL Version 2.1.


from dolfin import *; from mshr import *; 
from util_unicorn import *;
import time
import logging; logging.getLogger('FFC').setLevel(logging.WARNING)

#################################
## output files 

file_u = File("velocity.pvd")
file_p = File("pressure.pvd")
file_beta = File("beta_smooth.pvd")
file_theta = File("thetaf.pvd")

#################################




d = 2 # dimension of the problem

XMIN = 0.; XMAX = 4.; YMIN = 0; YMAX = 1.; G = [XMIN, XMAX, YMIN, YMAX]; eps = 1e-5 # Geometry and mesh

BARTHICK = 0.05
BARLENGTH = 1.0

CIRCLECENTREX = 0.5
CIRCLECENTREY = 0.5
CIRCLERADIUS = 0.1

BARLEFT = CIRCLECENTREX
BARRIGHT = CIRCLECENTREX + BARLENGTH
BARTOP = CIRCLECENTREY + BARTHICK/2.0
BARBOTTOM = CIRCLECENTREY - BARTHICK/2.0

BARGEOM = Rectangle(Point(BARLEFT, BARBOTTOM), Point(BARRIGHT, BARTOP))
CIRCLEGEOM = Circle(Point(CIRCLECENTREX, CIRCLECENTREY),CIRCLERADIUS)

BOXGEOM = Rectangle(Point(G[0], G[2]), Point(G[1], G[3])) - CIRCLEGEOM
BOXGEOM.set_subdomain(1,BARGEOM)
mesh = generate_mesh(BOXGEOM, 30)

for i in range(0, 2): # Refine mesh
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim());
    for c in cells(mesh):
        mp = c.midpoint()
        cell_markers[c] = abs(mp[0]) <= 1.5 and abs(mp[1] - (YMAX/2.0)) <= (0.3*YMAX) and c.diameter() > 0.05
	cell_markers[c] = cell_markers[c] or ((mp[0] < BARRIGHT) and (mp[0] > BARLEFT+ 0.15) and (mp[1] < BARTOP) and (mp[1] > BARBOTTOM))
    mesh = refine(mesh, cell_markers)

# FEM functions
V = VectorFunctionSpace(mesh, "CG", 1); Q = FunctionSpace(mesh, "CG", 1); W = V * Q; h = CellSize(mesh);
(v, q) = TestFunctions(W); w = Function(W); (u, p) = (as_vector((w[0], w[1])), w[2]); u0 = Function(V)



vert_to_dofs, dofs_to_vert, vectordofs_to_vert, vectordofs_to_subvert =  dofs_to_verts(mesh, Q, V)

DGv = VectorFunctionSpace(mesh, "DG", 0) 
DGm = VectorFunctionSpace(mesh, "DG", 0, d*d)
DG0 = FunctionSpace(mesh, "DG", 0)


#######################################
## phase function 

theta = Function(DG0)
theta.interpolate(Expression("1.0 - ((x[0] < BARRIGHT) && (x[0] > BARLEFT) && (x[1] < BARTOP) && (x[1] > BARBOTTOM))", BARLEFT=BARLEFT, BARRIGHT=BARRIGHT, BARTOP=BARTOP, BARBOTTOM=BARBOTTOM))
theta.set_allow_extrapolation(True)

######################################


######################################
## for debugging 
beta = Function(V)
beta.interpolate(Expression(("0", "0.02*((x[0] < BARRIGHT+eps) && (x[0] > BARLEFT-eps) && (x[1] < BARTOP+eps) && (x[1] > BARBOTTOM-eps))*(x[0] -  BARLEFT)"), eps=eps, BARLEFT=BARLEFT, BARRIGHT=BARRIGHT, BARTOP=BARTOP, BARBOTTOM=BARBOTTOM))
betasmooth = Function(V)
######################################

######################################
## solid stress 

S0 = Function(DGm)
dotS = Function(DGm)
Ss = Function(DGm)
#######################################

#######################################
## for debugging
betaf = File("beta.pvd")
beta.rename("beta", "label")
betaf << beta
######################################

solid_vertices = Function(Q)
solid_vertices.interpolate(Expression( "(x[0] < BARRIGHT+eps) && (x[0] > BARLEFT-eps) && (x[1] < BARTOP+eps) && (x[1] > BARBOTTOM-eps)", eps=eps, BARLEFT=BARLEFT, BARRIGHT=BARRIGHT, BARTOP=BARTOP, BARBOTTOM=BARBOTTOM))


####################################
## for mesh movement

mesh_u0 = Function(V)
tmp = Function(V)

###################################

uin = Expression(("2*4*(x[1]*(YMAX-x[1]))/(YMAX*YMAX)", "0."), YMAX=YMAX) # Inflow velocity
om = Expression("x[0] > XMAX - eps ? 1. : 0.", XMAX=XMAX, eps=eps) # Mark regions for boundary conditions
im = Expression("x[0] < XMIN + eps ? 1. : 0.", XMIN=XMIN, eps=eps)
nm = Expression("x[0] > XMIN + eps && x[0] < XMAX - eps ? 1. : 0.", XMIN=XMIN, XMAX=XMAX, eps=eps)

k = 0.00025; nu = 1e-6; d = .2*h**(3./2.) # Timestep, diffusion and stabilization parameters
t, T = 0., 1.; gamma = 10*1./h # Time interval and penalty parameter

mu = 30 # not sure

stepcounter = 0; timer0 = time.clock()

while t < T: # Time-stepping loop
    # Weak residual of stabilized FEM for Navier-Stokes eq.
    um = .5*(u + u0) # velocity u at midpoint of time interval

    mesh_u0.interpolate(Expression(("x[0]","x[1]"), degree=1))
  
    
    # prepare step
    betasmooth = laplacian_smooth(mesh, theta, u0, V)
    # move the mesh
    tmp.vector().set_local(np.array( mesh_u0.vector()) + k*np.array(betasmooth.vector()) )
    mesh_move(mesh, tmp, vectordofs_to_vert, vectordofs_to_subvert) 

    # Picard iteration
    up = u0
    pit = 0
    pincr = 1.
    while( pincr > 1e-3 ):

	# prepare iteration
    	# move structure 
    	#tmp.vector().set_local(np.array( mesh_u0.vector()) + k*np.array(up.vector()) )
    	#solid_move(mesh, tmp, vectordofs_to_vert, vectordofs_to_subvert, vert_to_dofs, solid_vertices) 

        # compute stress
    	tmp.vector().set_local(0.5*np.array( u0.vector()) + 0.5*np.array(up.vector()) )
	dotS = update_stress(mesh, tmp, S0, mu, DGm)	
	Ss = (S0 + k*dotS)
	Ssm = as_matrix([ [Ss[0], Ss[2]], [Ss[1], Ss[3]]])
	#

    	r = ((inner((u - u0)/k + grad(p) + theta*grad(um)*(up - betasmooth) , v) + theta*nu*inner(grad(um), grad(v)) + div(um)*q)*dx +
		(1-theta)*inner(Ssm, grad(v))*dx +
        	gamma*(om*p*q + im*inner(u - uin, v) + nm*inner(u, v))*ds + # Weak boundary conditions
        	d*(inner(grad(p) + grad(um)*(up - betasmooth ), grad(q) + grad(up)*v) + inner(div(um), div(v)))*dx) # Stabilization
    	solve(r==0, w)  # Solve the Navier-Stokes PDE (one timestep)
	up0 = up
	up  = project(u,V)
	pincr = norm(project(up - up0,V))
    	pit = pit + 1
	print "time ", t, " iteration ", pit, " updiff_l2norm ", pincr
	if pit > 10:
		print " convergence issue! , work on stabilization or timestep"
		exit()
     # end Picard iteration
    
    plot_compact(u, p,betasmooth, theta,  t, stepcounter, Q, V, file_u, file_p, file_beta, file_theta) # save all quantities 
    t += k; stepcounter += 1; 
    # Shift to next timestep
    u0 = project(u, V); 
    S0 = project(Ss, DGm);

print "elapsed CPU time: ", (time.clock() - timer0)
