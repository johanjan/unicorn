# Copyright (C) 2016-2017 Johan Jansson, Niyazi Cem Degirmenci and
# Frida Svelander
#
# Licensed under the GNU LGPL Version 2.1.

get_ipython().magic('matplotlib inline')
get_ipython().magic('run /home/fenics/fenics-matplotlib.py')
from dolfin import *; from mshr import *; 
import time
import logging; logging.getLogger('FFC').setLevel(logging.WARNING)
import numpy as np
from IPython.display import display, clear_output;
import time
import sys

# output files 
file_u = File("velocityCG1g.pvd")
file_p = File("pressureCG1g.pvd")
file_beta = File("beta_smoothCG1g.pvd")
file_theta = File("thetafCG1g.pvd")
file_beta = File("betaCG1g.pvd")


def dofs_to_verts(mesh, V, Vv):
    """ 
    Compute the dof to vert mapping
    original Function by Johan Hake from https://answers.launchpad.net/dolfin/+question/216695
    slightly modified by Cem
    """
    coords = mesh.coordinates()
    dim = mesh.geometry().dim()

    # Dof arrays
    vert_to_dofs = np.zeros(mesh.num_vertices(), dtype=np.uintp)

    dofs_to_vert = np.zeros(mesh.num_vertices(), dtype=np.uintp)
    vectordofs_to_vert = np.zeros((mesh.num_vertices()*dim), dtype=np.uintp)
    vectordofs_to_subvert = np.zeros((mesh.num_vertices()*dim),
dtype=np.uintp)

    # DofMaps
    dm = V.dofmap()
    dms = [Vv.sub(i).dofmap() for i in range(dim)]

    # Iterate over cells and collect dofs
    for cell in cells(mesh):
        cell_ind = cell.index()
        vert_inds = cell.entities(0)

        # Check that dof coordinates are the same as the vertex coordinates
        #assert(np.all(coords[vert_inds]==dm.tabulate_coordinates(cell)))
        dofs_to_vert[dm.cell_dofs(cell_ind)] = vert_inds
        vert_to_dofs[vert_inds] = dm.cell_dofs(cell_ind)

        # Iterate over the sub dof maps
        for i, dms_i in enumerate(dms):
            vectordofs_to_vert[dms_i.cell_dofs(cell_ind)] = vert_inds
            vectordofs_to_subvert[dms_i.cell_dofs(cell_ind)] = i 

    # Return dof to vertex mapping
    return vert_to_dofs, dofs_to_vert, vectordofs_to_vert, vectordofs_to_subvert


def mesh_move(m, beta, u, solid_vertices, vectordofs_to_vert, vectordofs_to_subvert):
    """ 
    function expects a mesh, a CG1v function with dim = mesh.geometry().dim() and 
    the maps that are calculated with dofs_to_verts utility
    moves the whole mesh to beta
    """
    betavec = np.array(beta.vector())
    svec = np.array(solid_vertices.vector())
    uvec = np.array(u.vector())
    for dof in range(betavec.size):
        dofbval = betavec[dof]
        dofuval = uvec[dof]
        dofsval = svec[dof]
        vertind = vectordofs_to_vert[dof]
        x_or_y  = vectordofs_to_subvert[dof]
        if dofsval == 0:
            m.coordinates()[vertind][x_or_y] = dofbval
        else:
            m.coordinates()[vertind][x_or_y] = dofuval

def laplacian_smooth(m, theta, flowvel, V ):
    """
    function to compute a smoothed mesh motion where 
    beta = u in solid part (theta = 0) and 
    (\nabla beta, \nabla v ) = 0  in fluid part
    """
    class All(SubDomain):
        def inside(self, x, on_boundary):
            return ( on_boundary)

    zerovel = Constant((0.,0.))
    allb = All()

    bc = DirichletBC(V, zerovel, allb)

    h = CellSize(m)
    vol = CellVolume(m)
    u = TrialFunction(V)
    v = TestFunction(V)
    a = vol*inner(grad(u), grad(v))*dx + inner(u,v)*(1e3/h)*(1. - theta) *dx
    L = inner(flowvel, v)*(1e3/h)*(1.-theta )*dx
    ux = Function(V)
    solve(a==L, ux , bc)
    return ux

def update_mesh(mesh, vectordofs_to_vert, vectordofs_to_subvert, theta, x0, u, V, k, mesh_x, solid_vertices):
    mesh_move(mesh, mesh_x0, mesh_x0, solid_vertices, vectordofs_to_vert, vectordofs_to_subvert) 
    betasmooth = laplacian_smooth(mesh, theta, u, V)
    mesh_x.vector().set_local(np.array(x0.vector()) + k*np.array(betasmooth.vector()) )
    mesh_xs.vector().set_local(np.array(x0.vector()) + k*np.array(u.vector()))
    mesh_move(mesh, mesh_x, mesh_xs, solid_vertices, vectordofs_to_vert, vectordofs_to_subvert) 

    return betasmooth
    

def plot_compact_jupyter(m, u, p, beta ,theta ,t, stepcounter, Q, V, file_u, file_p, file_beta, file_theta, s): # Compact plot utility function
    if stepcounter % 1 == 0:
        uEuclidnorm = project(sqrt(inner(u, u)), Q); ax.cla(); fig = plt.gcf(); fig.set_size_inches(16, 2)
        plt.subplot(1, 2, 1); mplot_function(uEuclidnorm); plt.title("Velocity") # Plot norm of velocity
        if t == 0.: plt.colorbar(); plt.axis(G)
        plt.subplot(1, 2, 2);
        plt.cla(); plt.triplot(mesh2triang(m), 'k-'); plt.title("Mesh") # Plot mesh
        plt.suptitle("FSI - t: %f %s" % (t, s)); plt.tight_layout(); clear_output(wait=True); display(pl)
    file_theta << theta
    file_u << u
    file_p << p
    file_beta << beta

d = 2 # dimension of the problem

XMIN = 0.; XMAX = 2.5; YMIN = 0; YMAX = 0.41; G = [XMIN, XMAX, YMIN, YMAX]; eps = 1e-5 # Geometry and mesh

BARTHICK = 0.02
BARLENGTH = 0.35

CIRCLECENTREX = 0.2
CIRCLECENTREY = 0.2
CIRCLERADIUS = 0.05

BARLEFT = CIRCLECENTREX
BARRIGHT = CIRCLECENTREX + CIRCLERADIUS + BARLENGTH
BARTOP = CIRCLECENTREY + BARTHICK/2.0
BARBOTTOM = CIRCLECENTREY - BARTHICK/2.0

BARGEOM = Rectangle(Point(BARLEFT, BARBOTTOM), Point(BARRIGHT, BARTOP))
CIRCLEGEOM = Circle(Point(CIRCLECENTREX, CIRCLECENTREY),CIRCLERADIUS)

BOXGEOM = Rectangle(Point(G[0], G[2]), Point(G[1], G[3])) - CIRCLEGEOM
BOXGEOM.set_subdomain(1,BARGEOM)
#mesh = generate_mesh(BOXGEOM, 50)
mesh = Mesh("mesh.xml")

#mesh = refine(mesh)

for i in range(0, 2): # Refine mesh
    cell_markers = MeshFunction("bool", mesh, mesh.topology().dim());
    for c in cells(mesh):
        mp = c.midpoint()
        cell_markers[c] = abs(mp[0] - 0.6) <= 0.5 and abs(mp[1] - (YMAX/2.0)) <= (0.3*YMAX) and c.diameter() > 0.025
    cell_markers[c] = cell_markers[c] or ((mp[0] < BARRIGHT) and (mp[0] > BARLEFT+ 0.15) and (mp[1] < BARTOP) and (mp[1] > BARBOTTOM))
    mesh = refine(mesh, cell_markers)

    
# FEM functions
V = VectorFunctionSpace(mesh, "CG", 1); Q = FunctionSpace(mesh, "CG", 1); Y = TensorFunctionSpace(mesh, "CG", 1);
W = MixedFunctionSpace([V, Q, Y])
h = CellSize(mesh);
(v, q, y) = TestFunctions(W);
w = Function(W);
(u, p, S) = (as_vector((w[0], w[1])), w[2], as_matrix([ [w[3], w[5]], [w[4], w[6]]]));
u0 = Function(V)
S0 = Function(Y)
wt = TrialFunction(W)
wp0 = Function(W)
wp = Function(W)

vert_to_dofs, dofs_to_vert, vectordofs_to_vert, vectordofs_to_subvert =  dofs_to_verts(mesh, Q, V)

DGv = VectorFunctionSpace(mesh, "DG", 0) 
DG0 = FunctionSpace(mesh, "DG", 0)

theta = Function(DG0) # phase function
theta.interpolate(Expression("1.0 - ((x[0] < BARRIGHT) && (x[0] > BARLEFT) && (x[1] < BARTOP) && (x[1] > BARBOTTOM))",
                             BARLEFT=BARLEFT, BARRIGHT=BARRIGHT, BARTOP=BARTOP, BARBOTTOM=BARBOTTOM))
theta.set_allow_extrapolation(True)
theta.rename("theta", "")

solid_vertices = Function(V)
solid_vertices.interpolate(Expression(("(x[0] < BARRIGHT+eps) && (x[0] > BARLEFT-eps) && (x[1] < BARTOP+eps) && (x[1] > BARBOTTOM-eps)",
                                       "(x[0] < BARRIGHT+eps) && (x[0] > BARLEFT-eps) && (x[1] < BARTOP+eps) && (x[1] > BARBOTTOM-eps)"),
                                      eps=eps, BARLEFT=BARLEFT, BARRIGHT=BARRIGHT, BARTOP=BARTOP, BARBOTTOM=BARBOTTOM))

# fields for mesh movement
mesh_x0 = Function(V)
mesh_x = Function(V)
mesh_xs = Function(V)

# Boundary markers
uin = Expression(("1.5*2*4*(x[1]*(YMAX-x[1]))/(YMAX*YMAX)", "0."), YMAX=YMAX) # Inflow velocity
om = Expression("x[0] > XMAX - eps ? 1. : 0.", XMAX=XMAX, eps=eps) # Mark regions for boundary conditions
im = Expression("x[0] < XMIN + eps ? 1. : 0.", XMIN=XMIN, eps=eps)
nm = Expression("x[0] > XMIN + eps && x[0] < XMAX - eps ? 1. : 0.", XMIN=XMIN, XMAX=XMAX, eps=eps)

# Problem parameters
hmin = mesh.hmin()
k = 2.*hmin; nu = 1e-3; du1 = 1.*h**2; dp1 = 1.*h**2 # Timestep, diffusion and stabilization parameters
t, T = 0., 20.; gamma = 1e3/h # Time interval and penalty parameter
mu0 = 2e3 # Stiffness
mu1 = 2e3
C1 = Constant(mu0/2.)

stepcounter = 0; timer0 = time.clock()

pl, ax = plt.subplots();
while t < T: # Time-stepping loop
    um = .5*(u + u0) # velocity u at midpoint of time interval
    Sm = .5*(S + S0)
    
    mesh_x0.interpolate(Expression(("x[0]","x[1]"), degree=1)) # Store mesh coordinates
    
    niter = 0
        
    epsu = 0.5*(grad(um) + transpose(grad(um))) # strain rate tensor
        
    #solve(r==0, w)  # Solve the Unified Continuum PDE (one timestep)
    for i in range(0, 20): # Newton fixed-point iteration

        up = project(um, V); 
        betasmooth = update_mesh(mesh, vectordofs_to_vert, vectordofs_to_subvert, theta, mesh_x0, up, V, k, mesh_x, solid_vertices)
        uale = theta*(um - betasmooth)    
       
        # Weak residual of stabilized FEM for Unified Continuum eq.
        r = ((inner((u - u0)/k + grad(p) + theta*grad(um)*(um - betasmooth) , v) + theta*nu*inner(grad(um), grad(v)) + div(um)*q)*dx +
            (1-theta)*inner(Sm, grad(v))*dx +
            (1-theta)*inner((S - S0)/k - (2*C1*epsu + grad(um)*Sm + Sm*transpose(grad(um))), y)*dx + # stress rate model
            (theta)*inner(S, y)*dx + # stress rate model
            gamma*(om*p*q + im*inner(u - uin, v) + nm*inner(u, v))*ds + # Weak boundary conditions
            dp1*(inner(grad(p), grad(q) + theta*grad(um)*v))*dx + # Stabilization
            du1*(inner(grad(um)*uale, grad(q) + theta*grad(um)*v) + inner(div(um), div(v)))*dx) # Stabilization
        
        a = derivative(r, w, wt) # Linearize
        L = -r + derivative(r, w, w)
        solve(a == L, w) # Solve linearized problem

        wp = project(w, W); 
        wpincr = norm(project(wp - wp0, W))
        wpnorm = norm(wp)
        if wpnorm > 1e-8:
            wpincr /= wpnorm
        wp0 = project(wp, W); 
        #print "wpincr: ", wpincr, " niter: ", niter

        niter += 1;
        
        if(wpincr < 1e-3):
            break        

    uincr = norm(project(u - u0, V))
    Snorm = norm(project(S, Y))
    betanorm = norm(project(betasmooth, V))
    up = project(u, V); up.rename("u", "")
    pp = project(p, Q); pp.rename("p", "")
    betasmooth.rename("beta", "")
    s = "uincr: %e Snorm: %e betanorm: %e niter: %d k: %e" % (uincr, Snorm, betanorm, niter, k)       
    plot_compact_jupyter(mesh, up, pp, betasmooth, theta,  t, stepcounter, Q, V, file_u, file_p, file_beta, file_theta, s) # plot quantities 
     
    t += k; stepcounter += 1; # Shift to next timestep
    u0 = project(u, V); 
    S0 = project(S, Y);
    
print "elapsed CPU time: ", (time.clock() - timer0)


