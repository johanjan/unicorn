// Copyright (C) 2008-2016 Johan Jansson and Niclas Jansson as main authors.

// Licensed under the GNU LGPL Version 2.1.
//
// This program solves the incompressible Navier-Stokes equations using
// least-squares-Galerkin stabilized FEM with a Schur-preconditioned fixed-point
// iteration between the momentunm and pressure equations and a do-nothing adaptive
// method.

#include "NSESolver.h"
#include <stdlib.h>
#include <stdio.h>

using namespace dolfin;

constexpr real bmarg = 1.0e-5 + DOLFIN_EPS;
constexpr real robj = 1. - bmarg;

constexpr real xmin = -12.5;
constexpr real xmax = 17.5;
constexpr real ymin = -100.;
constexpr real ymax = 100.;
constexpr real zmin = -5.;
constexpr real zmax = 5.;

constexpr real xboxmin = -5.;
constexpr real xboxmax = 5.;
constexpr real yboxmin = -5.;
constexpr real yboxmax = 5.;
constexpr real zboxmin = -3.;
constexpr real zboxmax = 3.;

constexpr real xbody = 0;
constexpr real ybody = 0;
constexpr real zbody = 0;

constexpr real inoffset = 1e-2;

real tFinal = 10.0;

constexpr real Uin = 1;
constexpr real Uin_dual = 1;

// HiLiftPW-3 angles: 4.36, 10.47, 14.54, 18.58, 20.59, and 21.57

real alpha = 0.0;

real trip_factor = 0.;

// Initial condition
class Force : public Function
{
public:
  Force(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;

    if(trip_factor > 0. && (fabs(x[0] - 2.4) < 1.0) && (fabs(x[1] - 0.0) < 0.25) && (fabs(x[2] - 1.2) < 1.1) && (x[2] >= 0.0))
    {
      real r = ((double) std::rand() / (RAND_MAX));
      r = 2*r - 1;
      values[1] = trip_factor * r / 1.0;
    }
  }
};

// Initial condition
class InitialVelocity : public Function
{
public:
  InitialVelocity(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;
  }
};

// Inflow velocity
class Inflow : public Function
{
public:
  Inflow(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = Uin * cos(M_PI / 180.0 * alpha);
    values[1] = Uin * sin(M_PI / 180.0 * alpha);
    values[2] = 0.0;
  }
};

// Sub domain for Inflow boundary condition
class InflowBoundary : public SubDomain
{
  bool inside(const real* x, bool on_boundary) const
  {
    return (on_boundary && (x[0] < xmax - 0.5) &&
      !(x[0] >= xboxmin + bmarg && x[0] <= xboxmax - bmarg &&
	x[1] >= yboxmin + bmarg && x[1] <= yboxmax - bmarg &&
	x[2] >= zboxmin + bmarg && x[2] <= zboxmax - bmarg));
  }
};

// Inflow velocity for the dual problem
class BodyDualInflow : public Function
{
public:
  BodyDualInflow(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = Uin_dual;
    values[1] = Uin_dual;
    values[2] = 0.;
  }
};

// Sub domain for Dirichlet boundary condition
class Body : public SubDomain
{
  bool inside(const real* x, bool on_boundary) const
  {
    return on_boundary && (x[0] >= xboxmin + bmarg && x[0] <= xboxmax - bmarg &&
			    x[1] >= yboxmin + bmarg && x[1] <= yboxmax - bmarg &&
			    x[2] >= zboxmin + bmarg && x[2] <= zboxmax - bmarg) &&
      (x[2] >= 0.0);
  }
};

// Inflow velocity for the dual problem
class DualInflow : public Function
{
public:
  DualInflow(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
      values[0] = 0.0;
      values[1] = 0.0;
      values[2] = 0.0;
  }
};

// Sub domain for Dirichlet boundary condition
class AllBoundary : public SubDomain
{
  bool inside(const real* x, bool on_boundary) const
  {
    return on_boundary;
  }
};

// Outflow pressure
class Outflow : public Function
{
public:
  Outflow(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;
  }
};

// Sub domain for Outflow boundary condition
class OutflowBoundary : public SubDomain
{
  bool inside(const real* x, bool on_boundary) const
  {
    return (on_boundary && (x[0] >= xmax - 0.5));
  }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public SubDomain
{
  bool inside(const real* x, bool on_boundary) const
  {
    return x[0] <= xmax - bmarg && on_boundary;
  }
};

// Sub domain for Slip boundary condition
class SlipBoundary : public SubDomain
{
  bool inside(const real* x, bool on_boundary) const
  {
    return on_boundary && (x[0] >= xboxmin + bmarg && x[0] <= xboxmax - bmarg &&
			    x[1] >= yboxmin + bmarg && x[1] <= yboxmax - bmarg &&
			    x[2] >= zboxmin + bmarg && x[2] <= zboxmax - bmarg);
  }
};

// Marker and orientation function for drag computation
class ThetaDrag : public Function
{
public:
  ThetaDrag(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;

    if((x[0] >= xboxmin + bmarg && x[0] <= xboxmax - bmarg &&
	x[1] >= yboxmin + bmarg && x[1] <= yboxmax - bmarg &&
	x[2] >= zboxmin + bmarg && x[2] <= zboxmax - bmarg) &&
       (x[2] >= 0.0))
    {
      values[0] = cos(M_PI / 180.0 * alpha);
      values[1] = sin(M_PI / 180.0 * alpha);
    }
  }

  uint dim(uint i) const
  {
    return 3;
  }
};

// Marker and orientation function for lift computation
class ThetaLift : public Function
{
public:

  ThetaLift(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;

    if((x[0] >= xboxmin + bmarg && x[0] <= xboxmax - bmarg &&
	x[1] >= yboxmin + bmarg && x[1] <= yboxmax - bmarg &&
	x[2] >= zboxmin + bmarg && x[2] <= zboxmax - bmarg) &&
       (x[2] >= 0.0))
    {
      values[0] = cos(M_PI / 180.0 * (alpha + 90));
      values[1] = sin(M_PI / 180.0 * (alpha + 90));
    }
  }

  uint dim(uint i) const
  {
    return 3;
  }
};

// Marker function for weak boundary condition
class SlipMarker : public Function
{
public:

  SlipMarker(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;

    if(x[0] >= xmin + bmarg && x[0] <= xmax - bmarg)
      values[0] = 0.0;
  }
};

// Dual volume source for momentum
class PsiMomentum : public Function
{
public:

  PsiMomentum(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;
  }

  uint rank() const
  {
    return 1;
  }

  uint dim(uint i) const
  {
    return 3;
  }
};

// Dual volume source for continuity
class PsiContinuity : public Function
{
public:

  PsiContinuity(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;
  }

  uint rank() const
  {
    return 0;
  }

  uint dim(uint i) const
  {
    return 0;
  }
};

// Dual boundary source for momentum
class BPsiMomentum : public Function
{
public:

  BPsiMomentum(Mesh& mesh) : Function(mesh) {}

  void eval(real* values, const real* x) const
  {
    values[0] = 0.0;
    values[1] = 0.0;
    values[2] = 0.0;
    if(x[0] >= xboxmin + bmarg && x[0] <= xboxmax - bmarg &&
       x[1] >= yboxmin + bmarg && x[1] <= yboxmax - bmarg &&
       x[2] >= zboxmin + bmarg && x[2] <= zboxmax - bmarg)
    {
      values[0] = 0.0;
    }
  }

  uint rank() const
  {
    return 1;
  }

  uint dim(uint i) const
  {
    return 3;
  }
};

int main(int argc, char* argv[])
{
  // Create mesh
  Mesh mesh("mesh.bin");
  //Mesh mesh("mesh.xml");
  
//   for(int i = 0; i < 1; i++)
//   {
//     MeshFunction<bool> cell_marker;
//     cell_marker.init(mesh, mesh.topology().dim());
//     cell_marker = false;
//     for (CellIterator cell(mesh); !cell.end(); ++cell)
//     {
//       Cell c = *cell;
      
//       if(c.diameter() > 0.1)
// 	cell_marker.set(c, true);
//     }
//     RivaraRefinement::refine(mesh, cell_marker);
//   }
//   File meshfile("refined_mesh.bin");
//   meshfile << mesh;
//   exit(0);

  // Rotate aircraft and smooth rest of mesh

  if(!ParameterSystem::parameters.defined("alpha"))
    dolfin_add("alpha", 0.0);
  if(!ParameterSystem::parameters.defined("cfl_target"))
    dolfin_add("cfl_target", 8.0);
  if(!ParameterSystem::parameters.defined("trip_factor"))
    dolfin_add("trip_factor", 0.0);
  if(!ParameterSystem::parameters.defined("T"))
    dolfin_add("T", 10.0);
  File paramfile("parameters.xml");
  paramfile >> ParameterSystem::parameters;
  alpha = (real) dolfin_get("alpha");
  real cfl_target = (real) dolfin_get("cfl_target");
  trip_factor = (real) dolfin_get("trip_factor");
  tFinal = (real) dolfin_get("T");

  cout << "trip_factor: " << trip_factor << endl;


  MeshGeometry& geometry = mesh.geometry();

  real r0 = 2.8;
  real rmax = 6.0;
  for (VertexIterator vi(mesh); !vi.end(); ++vi)
  {
    Point cp(6.25, 10.0, vi->point()[2]);

    Point pdiff = vi->point() - cp;

    real theta = alpha;
    real r = pdiff.norm();
    if(r < r0)
      theta = (0.0 - theta)*2*DOLFIN_PI/360.0;
    else if(r >= r0 && r < rmax)
      theta = (0.0 - theta)*2*DOLFIN_PI/360.0*(rmax - r) / (rmax - r0);
    else
      theta = 0.0*2*DOLFIN_PI/360.0;
      
    Point prot(pdiff[0]*cos(theta) - pdiff[1]*sin(theta),
	       pdiff[0]*sin(theta) + pdiff[1]*cos(theta));

    Point np = prot + cp;

//     for(unsigned int i = 0; i < mesh.topology().dim(); i++)
//     {
//       geometry.x(vi->index(), i) = np[i];
//     }
  }


  // Create boundary conditions
  InflowBoundary iboundary;
  Inflow inflow(mesh);

  //InitialVelocity u_i0(mesh);
  Inflow u_i0(mesh);

  Force f(mesh);

  AllBoundary aboundary;
  DualInflow dinflow(mesh);

  Body body;
  BodyDualInflow bodyDualInflow(mesh);

  OutflowBoundary oboundary;
  Outflow outflow(mesh);

  SlipBoundary sboundary;

#warning "unused variables"
  DirichletBoundary dboundary;

  ThetaDrag thetadrag(mesh);
  ThetaLift thetalift(mesh);
  SlipMarker sm(mesh);

  PsiMomentum psim(mesh);
  PsiContinuity psic(mesh);
  BPsiMomentum bpsim(mesh);

  DirichletBCList dbcs_m;
  dbcs_m.push_back(std::make_pair(&iboundary,&inflow));

  SlipBCList sbcs_m;
  sbcs_m.push_back(&sboundary);

  DirichletBCList dbcs_c;
  dbcs_c.push_back(std::make_pair(&oboundary,&outflow));

  DirichletBCList dbcs_dm;
#warning "order is critical here"
  dbcs_dm.push_back(std::make_pair(&aboundary,&dinflow));
  dbcs_dm.push_back(std::make_pair(&body,&bodyDualInflow));

  dolfin_set("output destination","silent");
  if(dolfin::MPI::processNumber() == 0)
    dolfin_set("output destination","terminal");

  cout << "Unicorn: aircraft simulation with alpha = " << alpha << endl;


  for(int i = 0; i < 100; i++)
  {
    real r = ((double) std::rand() / (RAND_MAX));
    r = 2*r - 1;

    cout << "rand test: " << r << endl;
  };

  NSESolver solver(
      mesh,
      dbcs_m,
      sbcs_m,
      dbcs_c,
      dbcs_dm,
      &u_i0,
      &thetadrag,
      &thetalift,
      &sm,
      &psim,
      &psic,
      &bpsim,
      &f,
      cfl_target
      );
  solver.setT(tFinal);
  solver.run();

  exit(0);

  return 0;
}
